from fastapi import FastAPI
import matplotlib.pylab as plt
from starlette.requests import Request
from starlette.responses import StreamingResponse, HTMLResponse
import io
from ct_image_preprocess import retrieve_image_as_tensor, extract_window


FILE_PATH = 'seg-lungs-LUNA16/1.3.6.1.4.1.14519.5.2.1.6279.6001.100225287222365663678666836860.mhd'
clim=(-1000.0, 300)

app = FastAPI()

@app.get("/scroll/{slice}",  response_class=HTMLResponse)
async def root(request: Request, slice: int):

    img = io.BytesIO()
    image = retrieve_image_as_tensor(FILE_PATH)

    _, axs = plt.subplots(2, 2, figsize=(12, 12))
    axs[0,0].imshow(image[slice], clim=clim, cmap='gray')
    axs[0,1].imshow(extract_window(image[:, slice], -600, 1500).T, cmap="gray", origin="lower")
    axs[1,0].imshow(image[:, :, slice], clim=clim, cmap='gray')
    plt.show()

    img.seek(0)
    return StreamingResponse(img, media_type="image/png")