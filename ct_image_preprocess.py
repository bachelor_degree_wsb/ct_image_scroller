import SimpleITK as sitk
import numpy as np


def retrieve_image_as_tensor(image_path: str) -> np.ndarray:
    image = sitk.ReadImage(image_path)
    ct_scan = sitk.GetArrayFromImage(image).astype(np.float32)

    ct_scan = np.clip(ct_scan, -1000, 1000)
    return ct_scan

def extract_window(slice: np.ndarray, level: int, window: int) -> np.ndarray:
   """
   Function to display an image slice
   Input is a numpy 2D array
   """
   max = level + window/2
   min = level - window/2
   return slice.clip(min,max)