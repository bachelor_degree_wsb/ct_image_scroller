FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

ENV PYTHONUNBUFFERED 1

RUN apt update && apt upgrade -y
RUN apt install -y make

COPY requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /app
COPY . .
